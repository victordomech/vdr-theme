//  GENERIC JS
(function($) {
    // --- GLOBAL VARS ---------------------------- 

		// SLIDES PER VIEW RESPONSIVE
		function slidesPerViewSwiper() {

			var w = window.innerWidth;

			if(w <= 5000 && w > 2559) {
				return 3;
			}else if (w <= 2559 && w > 1980) {
				return 3;
			} else if (w <= 1980 && w > 1440) {
				return 3;
			} else if (w <= 1440 && w > 1366) {
				return 3;
			} else if (w <= 1366 && w > 1024) {
				return 2;
			} else if (w <= 1024 && w > 980) {
				return 2;
			} else if (w <= 980 && w > 800) {
				return 2;
			} else if(w <= 800){
				return 1;
			} 

		}
    // END GLOBAL VARS -----------------------------




    // --- GLOBAL FUNCTIONS ---------------------------- 


    /*********

        Function: hidePreloader
        sub-Function of vdr_loader.
        Victor Domenech Rodriguez.
        Version: 1.0

    *********/

    function hidePreloader() {
    	var preloaderFadeOutTime = 500;
        var preloader = $('.c-spinner-wrapper');
        preloader.fadeOut(preloaderFadeOutTime);
    }
    /*********

    menu_interaction
    Toggle the class active for open or close mobile menu
    Domenech
    1.0

    *********/

    	function menu_interaction(){
			$('.js-hamburger').on( "click", function() {
			  	$('.js-hamburger').toggleClass('is-active');
			  	$('.js-mobile-menu').toggleClass('is-active');
			});
		}

	/*********

    swiper_home
    Init Slider swiper of home.
    Domenech
    1.0

    *********/

    	function swiper_home(){
			var homeSwiper = new Swiper('.js-home-slider', {
			  	effect: 'fade',
			  	autoplay: 2500,
			  	autoplayDisableOnInteraction: false,
			  	slidesPerView: 1, 

			  	navigation: {
			        nextEl: '.js-home-slider__arrow-next',
			        prevEl: '.js-home-slider__arrow-prev',
			    },
			});
		}


	/*********

    setup_collapsible_submenus
    Do a collapsed menu in mobile.
    Domenech
    1.0

    *********/

	    function setup_collapsible_submenus() {
	        var $menu = $('.c-mobile-menu__menu'),
	            top_level_link = '.c-mobile-menu__menu .menu-item-has-children:not(.wpml-ls-item ) > a';
	             
	        $menu.find('a').each(function() {
	            $(this).off('click');
	              
	            if ( $(this).is(top_level_link) ) {
	                $(this).after('<a class="js-mobile-menu__sub-menu c-mobile-menu__menu-arrow" href="#"><i class="fas fa-arrow-down"></i></a>');
	            }
	 
	            if ( $(this).siblings('.sub-menu').length ) {

	            	$(this).next().on('click', function(event) {
	                    //event.preventDefault();
	                    $(this).parent().toggleClass('is-visible');
	                    $(this).siblings('.sub-menu').slideToggle( "slow");
	                });
	            }
	        });
	    }



        function call_fn(selector, fn, args){ if( $(selector).length>0 ) fn(args); }


    // END GLOBAL FUNCTIONS ---------------------------- 



    // --- READY --------------------------------------
    $(function () { 
		call_fn('.js-hamburger', menu_interaction);
		call_fn('.js-home-slider', swiper_home);

        // --- ON RESIZE WINDOW EVENT --------------------------------
        $(window).resize(function(){


        });
        // END ON RESIZE WINDOW EVENT --------------------------------

        // --- ON LOAD WINDOW EVENT --------------------------------
        $(window).on("load", function() {

            hidePreloader();
            setup_collapsible_submenus();
        });

        // END ON LOAD WINDOW EVENT --------------------------------

    });
    // END ON LOAD --------------------------------------

})(jQuery);
