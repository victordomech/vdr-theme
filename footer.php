<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package "theme"
 */

$classesAnimationFooter 	= " fadeIn animated";
$frontpage_ID 	= get_option('page_on_front');

		?>

				</main>

				<!-- FOOTER -->
				<!-- <footer class="c-footer <?php echo $classesAnimationFooter; ?>">

					<div class="c-footer__wraper">

						<div class="o-container">

							<div class="">
								<?php include(locate_template('template-parts/template-part-footer-menu.php')); ?>
							</div>

						</div>

						<div class="">

							<div class="">
								<?php include(locate_template('template-parts/template-part-footer-menu-alt.php')); ?>
							</div>

						</div>

						<div class="o-container ">
							<div class="c-footer__by">
								<a href="<?php echo DESIGN_BY; ?>" target="_blank"><?php echo DESIGN_BY_TEXT; ?></a>
							</div>
						</div>

				    </div>

				</footer>
				-->

			</div>

		</div>

		<!-- BUTTON TO TOP -->
		<div class="c-button-to-top js-button-to-top">
			<span class="fa fa-angle-up"></span>
		</div>

		<!-- SCRIPTS -->
		<div class="o-scripts">
			<?php wp_footer(); ?>
		</div>

	</body>
</html>