<?php

/**
 * *****************************************************************************
 * Disable wp options
 * *****************************************************************************
 */
// DISABLE GUTTENBERG EDITOR
add_filter('use_block_editor_for_post_type', '__return_false', 100);

// REMOVE SHOW ADMIN BAR
add_filter('show_admin_bar', '__return_false', 100);

// DISABLE AUTOMATIC UPDATER WP
add_filter( 'automatic_updater_disabled', '__return_true' );


/**
 * *****************************************************************************
 * Define custom fields
 * *****************************************************************************
 */

// DEVELOPMENT MODE
define ( "DEVELOPMENT_MODE", false);

// DEBUG fields
define ( "DEBUG_ADMIN", false );

// DESIGN BY
define ( "DESIGN_BY", "###");

// DESIGN BY TEXT
define ( "DESIGN_BY_TEXT", "DESIGN_BY_TEXT");

// THEME fields
$theme = wp_get_theme();
define ( "THEME_NAME", $theme->get( 'Name' ) );
define ( "THEME_TITLE", ucfirst($theme->get( 'Name' )) );

// URL PATHS
define ( "CONFIG_DIRECTORY", get_template_directory_uri() . "/config" );
define ( "ASSETS_DIRECTORY", get_template_directory_uri() . "/assets" );
define ( "TEMPLATE_PARTS_DIRECTORY", get_template_directory_uri() . "/template-parts" );
define ( "IMAGES_DIRECTORY", ASSETS_DIRECTORY . "/images" );
define ( "CSS_DIRECTORY", ASSETS_DIRECTORY . "/css" );
define ( "JS_DIRECTORY", ASSETS_DIRECTORY . "/js" );

// SERVER PATHS
define ( "SERVER_CONFIG_DIRECTORY", get_template_directory() . "/config" );
define ( "SERVER_ASSETS_DIRECTORY", get_template_directory() . "/assets" );
define ( "SERVER_TEMPLATE_PARTS_DIRECTORY", get_template_directory() . "/template-parts" );
define ( "SERVER_IMAGES_DIRECTORY", SERVER_ASSETS_DIRECTORY . "/images" );
define ( "SERVER_CSS_DIRECTORY", SERVER_ASSETS_DIRECTORY . "/css" );
define ( "SERVER_JS_DIRECTORY", SERVER_ASSETS_DIRECTORY . "/js" );

// POSTS PER PAGE
define("VDR_POST_PER_PAGE", 10);

// DEFINE VDR USER ID ADMIN
define( "VDR_USER_ID", 1);

/**
 * *****************************************************************************
 * Require files
 * *****************************************************************************
 */

// TGM config
require_once get_template_directory() . '/config/wp-require-plugins.php';

// GLOBAL VAR ARRAY ANIMATIONS
require_once get_template_directory() . '/config/vdr-animations.php';

// NEED INSTALLED ACF
if(class_exists('acf')){

	// ADD PAGES OF THEME SETTINGS
	require_once get_template_directory() . '/config/vdr-acf-theme-settings.php';

}

// Remove WP Version From Styles
add_filter( 'style_loader_src', 'sdt_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999 );
 
// Function to remove version numbers
function sdt_remove_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

 
/**
 * *****************************************************************************
 * Favicon setup
 * *****************************************************************************
 */
function favicon() {

	if(class_exists('acf')){

		$url_favicon 		= get_field("theme_settings_general__favicon", "option");
		$favicon 			= ($url_favicon) ? $url_favicon["sizes"]["large"] : IMAGES_DIRECTORY . '/favicon.ico';

		echo '<link rel="shortcut icon" href="' . $favicon  . '" />';

	}

}

/**
 * *****************************************************************************
 * Login setup
 * *****************************************************************************
 */
function my_login_logo() {

	if(class_exists('acf')){

		$url_logo 			= get_field("theme_settings_general__logo", "option");
		$logo 				= ($url_logo) ? $url_logo["sizes"]["large"] : IMAGES_DIRECTORY . '/logo.png';

		$background_image_login = get_field("theme_settings_general__login_bg_image", "option");
		if($background_image_login) $background_image_login = $background_image_login["sizes"]["large"];
		$background_image_register = get_field("theme_settings_general__register_bg_image", "option");
		if($background_image_register) $background_image_register = $background_image_register["sizes"]["large"];

		favicon();

		?>
	    <style type="text/css">
	    	/* Custom login */
	        body.login div#login h1 a {
	            width: 100%;
				background: url(<?php echo $logo; ?>) no-repeat;
	        	margin-bottom: 10px;
	        	background-position: center;
	        	background-size: contain;
	        	height: 140px;
	        }

	        body.login  #backtoblog,  body.login  #nav{
	        	padding: 15px;
	        	background-color: rgba(255,255,255, 1);
	        }
	        body.login  #backtoblog{
	        	margin-top: 0;
	        }

	        body.login #backtoblog a, body.login #nav a{
	        	color: #72777c;
	        	font-size: 1.2em;
	        }

	        body.login #backtoblog a:hover, body.login #nav a:hover {
			    color: #00a0d2;
			}

			html{
				background: none!important;
			}

			html, body.login{
				height: auto!important;
				min-height: 100vh;
			}

	        body.login.login-action-login{
	        	background: url(<?php echo $background_image_login; ?>) no-repeat center center;
	        	background-size: cover;
	        	background-attachment: fixed; 
	        }

	        body.login.login-action-register{
	        	background: url(<?php echo $background_image_register; ?>) no-repeat center center;
	        	background-size: cover;
	        	background-attachment: fixed;
	        }
	    </style>
		<?php
	}
}

add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo_url() {return home_url();}
function my_login_logo_url_title() {return THEME_TITLE;}
add_filter( 'login_headerurl', 'my_login_logo_url' );
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


/**
 * *****************************************************************************
 * Setup
 * *****************************************************************************
 */
remove_action('wp_head', 'wp_generator');
add_action('admin_head','favicon');

if (! function_exists ( 'theme_setup' )) :
	function theme_setup() {
		
		// Load text domain
		load_theme_textdomain ( THEME_NAME, get_template_directory () . '/languages' );
		// Add theme supports
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support('post-thumbnails');
		
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
		) );
		
		/*
		 * Enable support for Post Formats.
		 * See https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		add_theme_support( 'post-formats', array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
		) );
		
		add_theme_support('woocommerce');

		// instanciate email (NEED INSTALLED ACF)
		if(class_exists('VDR_Email')){

			$GLOBALS['vdr_emails'] = VDR_Email::instance(null);

		}
		
	}
endif;
add_action ( 'after_setup_theme', 'theme_setup' );


if (! function_exists ( 'theme_admin_setup' )) :
	function theme_admin_setup() {
		// Restrict admin page
		if ( ! current_user_can( 'manage_options' ) && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
			wp_redirect( home_url() );
			exit;
		} else {
			// Add capabilities

		}
	}
endif;
add_action( 'admin_init', 'theme_admin_setup', 1 );

/**
 * *****************************************************************************
 * Theme admin debug
 * *****************************************************************************
 */
if (DEBUG_ADMIN) {
	if (!function_exists('debug_admin_menus')):
		function debug_admin_menus() {
			if ( !is_admin())
				return;
			global $submenu, $menu, $pagenow;
			if ( current_user_can('manage_options') ) {
				if( $pagenow == 'index.php' ) {
					echo '<pre>'; print_r( $menu ); echo '</pre>';
					echo '<pre>'; print_r( $submenu ); echo '</pre>';
				}
			}
		}
		add_action( 'admin_notices', 'debug_admin_menus' );
	endif;
}

/**
 * *****************************************************************************
 * Enqueue scripts and styles
 * *****************************************************************************
 */
function custom_admin_enqueue_scripts() {

	wp_enqueue_style( 'font-awesome', 	"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css"  );

	// CUSTOM ADMIN JS
	wp_enqueue_script( 'custom-admin-js', 	JS_DIRECTORY . "/min/custom-admin.min.js", array( 'jquery', 'jquery-ui-tabs', 'jquery-ui-datepicker' ), '', true );
	
}
add_action ( 'admin_enqueue_scripts', 'custom_admin_enqueue_scripts' );

function custom_wp_enqueue_scripts() {

	// ANIMATE CSS
	wp_enqueue_style( 'animate', 		"https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css" );

	// SWIPER CSS
	wp_enqueue_style( 'swiper', 		ASSETS_DIRECTORY . "/vendor/swiper/swiper.min.css" );

	// STYLE CSS
	//wp_enqueue_style( 'style', 			get_template_directory_uri() . "/style.css" );

	// MAIN CSS
	wp_enqueue_style( 'main', 			CSS_DIRECTORY . "/main.css", false, filemtime( SERVER_CSS_DIRECTORY . "/main.css" ) );


	// JS

	// SWIPER JS
	wp_enqueue_script( 'swiper',  		ASSETS_DIRECTORY . "/vendor/swiper/swiper.min.js", array('jquery'), true);

	// FONTANIMATE JS
	//wp_enqueue_script( 'vivus',  		ASSETS_DIRECTORY . "/vendor/vivus/vivus.min.js", array('jquery'), true);

	// CUSTOM JS
	wp_enqueue_script( 'customJS', 		JS_DIRECTORY . "/min/custom.min.js", array( 'jquery', 'swiper' ), true ); 

}

add_action ( 'wp_enqueue_scripts', 'custom_wp_enqueue_scripts' );


function dequeue_divi_css() {
	if ( is_single() && 'post' == get_post_type() ) {
		wp_enqueue_style('et-builder-modules-style');
	}
	else{
	  	wp_dequeue_style('et-builder-modules-style');
	}
}
add_action('wp_enqueue_scripts','dequeue_divi_css', 100);

/**********PRELOADER**********/

function vdr_loader() {

	ob_start();

    get_template_part('template-parts/spinner'); 

	echo ob_get_clean();

}

add_action( 'after_body', 'vdr_loader' );


/**
 * ***********************************************************************
 * Register menus
 * ****
 *************************************************************************
 */
if (! function_exists ( 'custom_navigation_menus' )) :
	function custom_navigation_menus() {
		$locations = array(
				'header_menu' 		=> __( 'Header Menu', THEME_NAME ),
				'header_menu_secundary' 	=> __( 'Header Menu secundary', THEME_NAME ),
				'footer_menu' 		=> __( 'Footer Menu', THEME_NAME ),
				'footer_menu_secundary' 	=> __( 'Footer Menu secundary', THEME_NAME ),
		);
		register_nav_menus( $locations );
	}
	add_action ( 'init', 'custom_navigation_menus' );
endif;
/**
 * *****************************************************************************
 * Register sidebars
 * *****************************************************************************
 */
if (! function_exists ( 'custom_sidebars' )) {
	function custom_sidebars() {
		register_sidebar( array(
	        'name' => __( 'Blog Sidebar', THEME_NAME ),
	        'id' => 'vdr-sidebar-custom',
	        'description' => __( 'Widgets in this area will be shown on all posts and pages.', THEME_NAME ),
	        'before_widget' => '<div id="%1$s" class="sidebar-block %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="sidebar-title">',
			'after_title'   => '</h3>',
	    ) );
	}
	add_action ( 'widgets_init', 'custom_sidebars' );
}


/**
 * *****************************************************************************
 * Customize wp_head function; add google analytics, facebook, twitter api, etc
 * *****************************************************************************
 */
function custom_wp_head() {
	
	$google_analytics = get_field("theme_settings_general__google_analytics", "option");

	if($google_analytics){
		?>
		<!-- [google_analytics] begin -->

		<?php echo $google_analytics; ?>
		
		<!-- [google_analytics] end -->
		<?php
	}
	
}
add_action ( 'wp_head', 'custom_wp_head' );

/**
 * *****************************************************************************
 * Excerpt Lenght
 * *****************************************************************************
 */
function custom_excerpt_length($length) {
	return 40;
}
// add_filter ( 'excerpt_length', 'custom_excerpt_length', 99 );

/**
 * *****************************************************************************
 * Custom Functions
 * *****************************************************************************
 */

// function to check if device is a mobile.
function is_mobile() {
	return (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet'.
			'|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]'.
			'|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT'] );
}

// function to add classes in body of browser and OS
function browser_body_class($classes) {
	global $is_lynx, $is_gecko, $is_IE, $is_edge, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

	if($is_lynx) $classes[] = 'lynx';
	elseif($is_gecko) $classes[] = 'gecko';
	elseif($is_opera) $classes[] = 'opera';
	elseif($is_NS4) $classes[] = 'ns4';
	elseif($is_safari) $classes[] = 'safari';
	elseif($is_chrome) $classes[] = 'chrome';
	elseif($is_IE) $classes[] = 'ie';
	elseif($is_edge) $classes[] = 'edge';
	else $classes[] = 'unknown';

	if($is_iphone) $classes[] = 'iphone';


	if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
		$classes[] = 'osx';
	} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
		$classes[] = 'linux';
	} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
		$classes[] = 'windows';
	}

	// IS RENDER WEBKIT
	if( preg_match('/webkit/', strtolower($_SERVER['HTTP_USER_AGENT']) ) ) { 
	    $classes[] = 'webkit'; 
	}

	if(is_mobile()) $classes[] = 'is_mobile';

	return $classes;
}
add_filter('body_class','browser_body_class');


// DISPLAY CORRECTLY TAXS IN BACKEND
add_filter( 'wp_terms_checklist_args', 'checked_not_ontop', 1, 2 );
function checked_not_ontop( $args, $post_id ) {

	// IF NEED SPECIFICATION
    if ( 'cpt' == get_post_type( $post_id ) && $args['taxonomy'] == 'custom_tax' ) $args['checked_ontop'] = false;

    // DEFAULT
	$args['checked_ontop'] = false;

    return $args;

}


// DETERMINE THE TOP MOST PARENT OF A TERM
function get_term_top_most_parent($term_id, $taxonomy){
    // start from the current term
    $parent  = get_term_by( 'id', $term_id, $taxonomy);
    // climb up the hierarchy until we reach a term with parent = '0'
    while ($parent->parent != '0'){
        $term_id = $parent->parent;

        $parent  = get_term_by( 'id', $term_id, $taxonomy);
    }
    return $parent;
}

// DEBUG IN HTML
function dump($var){

	echo "<!-- <pre>" . var_dump($var) . "</pre>  -->";
	
}

// RETURN FORMATTED DATE
function vdr_format_date($date, $format){
	// extract Y,M,D
	$y = substr($date, 0, 4);
	$m = substr($date, 4, 2);
	$d = substr($date, 6, 2);

	// create UNIX
	$time = strtotime("{$d}-{$m}-{$y}");

	// return format date
	return date($format, $time);
}

/**
 * *****************************************************************************
 * Theme custom functions
 * *****************************************************************************
 */


function custom_pagination_base() {
	global $wp_rewrite;

  	// Translate
	$wp_rewrite->pagination_base = __("page", THEME_NAME);

	$wp_rewrite->flush_rules();

}
add_action( 'init', 'custom_pagination_base', 1 );


function vdrBreakLines($content){

	$content = apply_filters( 'the_content', $content );
    $content = str_replace( ']]>', ']]&gt;', $content );

    return $content;

} 

// VDR - CONTACT FORM 7
// NO PRINT CONTENT IN <span>[_your-field] YOUR FIELD: [your-field][_your-field]</span>
// IF NOT EXIST [your-field]
add_filter( 'wpcf7_special_mail_tags', 'empty_field_characters',10,2);
function empty_field_characters( $output,$name ) {
	$name=trim(ltrim($name,'_'));
	if(empty($_POST[$name])){
		$output='{}';
	} else $output=' ';
	return $output;
}

//filter to remove any p tags that contain the special characters for an empty field</p>
add_filter( 'wpcf7_mail_components', 'remove_blank_lines' );
function remove_blank_lines( $mail ) {
	if ( is_array( $mail ) && ! empty( $mail['body'] ) )
		$mail['body'] = preg_replace('/<span\b[^>]*>{}(.*?){}<\/span>/i', '', $mail['body'] );
	return $mail;
}

// DEBUG ONLY FOR AUTHOR VDR
function vdr_dump($var){

	$current_user = wp_get_current_user();

	if ( VDR_USER_ID == $current_user->ID ) {

	    // ONLY AUTHOR VDR
	    var_dump($var);
	    
	}

}


// DISABLE NOTIFICATIONS FOR THEMES, WP CORE AND PLUGINS
function remove_core_updates(){

	global $wp_version;
	return(object) array('last_checked'=> time(), 'version_checked'=> $wp_version);

}

// ONLY DISABLE IF USER IS NOT VDR
function vdr_disable_update_notifications(){

	$current_user = wp_get_current_user();

	if ( VDR_USER_ID != $current_user->ID ) {

		add_filter('pre_site_transient_update_core', 'remove_core_updates');
		add_filter('pre_site_transient_update_plugins', 'remove_core_updates');
		add_filter('pre_site_transient_update_themes', 'remove_core_updates');

	}

}
add_action('after_setup_theme', 'vdr_disable_update_notifications');


// Our filter callback function
add_filter( 'cn_cookie_notice_output', 'vdr_cookie_notice', 10, 3 );
function vdr_cookie_notice($output, $options ) {

    // message output

    $frontpage_ID 	= get_option('page_on_front');
    $text_cookie_notice = get_field("page_template_home_text_cookie", $frontpage_ID);
    $button_text = get_field("page_template_home_text_button_cookie", $frontpage_ID);

    if(!$text_cookie_notice) 
    	$text_cookie_notice = __( 'We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.', THEME_NAME );

    if(!$button_text) $button_text = __('I agree.', THEME_NAME);
	
	$output = '
	<div id="cookie-notice" class="cn-' . ($options['position']) . ($options['css_style'] !== 'none' ? ' ' . $options['css_style'] : '') . '" style="color: ' . $options['colors']['text'] . '; background-color: ' . $options['colors']['bar'] . ';">'
		. '<div class="cookie-notice-container"><span id="cn-notice-text">'. $text_cookie_notice . '</span>'
		. '<a href="#" id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie button' . ($options['css_style'] !== 'none' ? ' ' . $options['css_style'] : '') . '">' . $button_text . '</a>'
		. ($options['refuse_opt'] === 'yes' ? '<a href="#" id="cn-refuse-cookie" data-cookie-set="refuse" class="cn-set-cookie button' . ($options['css_style'] !== 'none' ? ' ' . $options['css_style'] : '') . '">' . $button_text . '</a>' : '' )
		. ($options['see_more'] === 'yes' ? '<a href="' . ($options['see_more_opt']['link_type'] === 'custom' ? $options['see_more_opt']['link'] : get_permalink( $options['see_more_opt']['id'] )) . '" target="' . $options['link_target'] . '" id="cn-more-info" class="button' . ($options['css_style'] !== 'none' ? ' ' . $options['css_style'] : '') . '">' . $button_text . '</a>' : '') . '
		</div>
	</div>';

    return $output;

}

// CHECK IF STRING CONTAINS SUBSTRING
// RETURN FALSE / TRUE
function vdr_string_contains_substring($str, $substr){

	if (strpos($str, $substr) !== false) return true;
	return false;

}

// EXAMPLE USAGE: $page_id = vdr_get_page_id_by_template('templates/vdr-template-contact.php');
// RETURN FIRST ID PAGE WITH TEMPLATE $slug_template
function vdr_get_page_id_by_template($slug_template){

	// SLUG TEMPLATE EXAMPLE: templates/vdr-template-contact.php

	// GET PAGES
	$args = [
	    'post_type' => 'page',
	    'fields' => 'ids',
	    'nopaging' => true,
	    'meta_key' => '_wp_page_template',
	    'meta_value' => $slug_template
	];
	$pages = get_posts( $args );
	if(is_array($pages) && count($pages)>0) return $pages[0];
	return false;

}

// Breadcrumbs
function vdr_custom_breadcrumbs( $separator = '&gt;', $breadcrums_id = 'vdr-breadcrumbs', $breadcrums_class  = 'vdr-breadcrumbs', $home_title = false) {

	if(empty($home_title)) $home_title = get_the_title(get_option('page_on_front'));
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {


			$current_term = get_queried_object();

        	if($current_term->parent != 0){

        		$parent = get_term_by('term_id', $current_term->parent, 'category');
        		$cat_id         = $parent->term_id;
                $cat_nicename   = $parent->slug;
                $cat_link       = get_term_link($parent->term_id, 'category');
                $cat_name       = $parent->name;

        		// Parent term
        		echo '<li class="item-cat"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
        		// Separator
        		echo '<li class="separator"> ' . $separator . ' </li>';

        		// Category page child (current)
            	echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

        	}else{

        		// Category page parent
            	echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

        	}
               
            
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}

function mytheme_comment($comment, $args, $depth) {

    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    
    ?>
    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-author vcard">
        <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
        <?php printf( __( '<cite class="fn">%s</cite> <span class="says">says:</span>' ), get_comment_author_link() ); ?>
    </div>
    <?php if ( $comment->comment_approved == '0' ) : ?>
         <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', THEME_NAME ); ?></em>
          <br />
    <?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
        </a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );

        ?>
    </div>

    <?php comment_text(); ?>

    <div class="reply">
        <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
    <?php

}


/**
 * *****************************************************************************
 * Custom post types
 * *****************************************************************************
 */

if ( ! function_exists('vdr_theme_post_type') ) {
	function vdr_theme_post_type() {

		// Create custom post types


		// flush_rewrite_rules();
		
	}
	add_action( 'init', 'vdr_theme_post_type', 0 );
}

// SECURITY - BLOCK ACCESS TO URL AUTHOR PARAMETER
if (!is_admin()) {
	// default URL format
	if (preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING'])) die();
	add_filter('redirect_canonical', 'shapeSpace_check_enum', 10, 2); } function shapeSpace_check_enum($redirect, $request) {
	// permalink URL format
	if (preg_match('/\?author=([0-9]*)(\/*)/i', $request)) die();
	else return $redirect;
}

// LOGIN ERROR MESSAGE
function login_error_message($error){

    //its the right error so you can overwrite it
    $error = __("Wrong information", THEME_NAME);
    return $error;

}
add_filter('login_errors','login_error_message');
