const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const cssnano = require('gulp-cssnano');
const uglify = require('gulp-uglify');
const pump = require('pump');
const concat = require('gulp-concat');
const rename = require('rename');

const route = "assets";
const node = "./node_modules/";

const config = {
	dev: {
		uglify: {
			mangle: false
		},
		sass: {
			precision: 6
		},
		cssnano: {
			autoprefixer: {
				add: true,
				browsers: ['> 3%', 'last 2 versions', 'ie > 9', 'ios > 5', 'android > 3']
			},
			zindex: false,
            discardUnused: {fontFace: false},
            minifyFontValues: false,
            reduceIdents: false,
            reducePositions: false
		}
	},
	dist: {
		uglify: {
			mangle: true
		}
	}

};

/**********************************************************/
/************************ DEFAULT *************************/
/**********************************************************/



function sass_base() {
	return gulp
        .src(route + '/sass/*.scss')
		.pipe(sourcemaps.init())
    	.pipe(sass(config.dev.sass).on('error', sass.logError))
		.pipe(cssnano(config.dev.cssnano))
		.pipe(sourcemaps.write('.'))
    	.pipe(gulp.dest(route + '/css'));
}

function custom_js() {
    return gulp
        .src(route + '/js/*.js')
        .pipe(concat('custom.min.js'))
        .pipe(uglify(config.dev.uglify))
        .pipe(gulp.dest(route + '/js/min'));
}

function custom_js_admin() {
	return gulp
        .src(route + '/js/admin/*.js')
        .pipe(concat('custom-admin.min.js'))
        .pipe(uglify(config.dev.uglify))
        .pipe(gulp.dest(route + '/js/min'));
}

function watch() {
	gulp.watch(route + '/sass/*.scss', gulp.series('sass_base'));
	gulp.watch(route + '/js/*.js', gulp.series('custom_js'));
	gulp.watch(route + '/js/admin/*.js', gulp.series('custom_js_admin'));
}

function fontawesome_font() {
 
// Copy all Font Awesome Fonts
    return gulp
        .src( node + '@fortawesome/fontawesome-free/webfonts/*' )
        .pipe( gulp.dest( route + '/fonts' ) );
}

function fontawesome_css() {
// Copy all Font Awesome SCSS files
    return gulp
        .src( node + '@fortawesome/fontawesome-free/scss/*' )
        .pipe( gulp.dest( route + '/vendor/fontawesome' ) );
}

function normalize_css() {
// Copy and minify Normalize CSS files
    return gulp
        .src( node + 'normalize.css/normalize.css' )
        .pipe(cssnano(config.dev.cssnano))
        .pipe( gulp.dest( route + '/vendor/normalize' ) );
}


function hamburgers() {
// Copy hamburguers
    return gulp
        .src( node + 'hamburgers/_sass/hamburgers/*' )
        .pipe( gulp.dest( route + '/vendor/hamburgers' ) );
}

function hamburgers_types() {
// Copy hamburguers
    return gulp
        .src( node + 'hamburgers/_sass/hamburgers/types/*' )
        .pipe( gulp.dest( route + '/vendor/hamburgers/types' ) );
}


function swiper_css() {
// Copy swiper css
    return gulp
        .src( node + 'swiper/css/swiper.min.css' )
        .pipe( gulp.dest( route + '/vendor/swiper/' ) );
}


function swiper_js() {
// Copy swiper js
    return gulp
        .src( node + 'swiper/js/swiper.min.js' )
        .pipe( gulp.dest( route + '/vendor/swiper/') );
}


function vivus() {
// Copy swiper js
    return gulp
        .src( node + 'vivus/dist/vivus.min.js' )
        .pipe( gulp.dest( route + '/vendor/vivus/') );
}


const stylesheets = gulp.series( sass_base, fontawesome_css, fontawesome_font, hamburgers, hamburgers_types, swiper_css, swiper_js, vivus);
const scripts = gulp.parallel(custom_js, custom_js_admin);

const build = gulp.series( gulp.parallel(stylesheets,scripts), watch);

exports.sass_base = sass_base;
exports.custom_js = custom_js;
exports.custom_js_admin = custom_js_admin;
exports.watch = watch;
exports.stylesheets = stylesheets;
exports.scripts = scripts;
exports.fontawesome_css = fontawesome_css;
exports.fontawesome_font = fontawesome_font;
exports.default = build;
