<?php

$url_logo 			= get_field("theme_settings_general__logo", "option");
$logo 				= ($url_logo) ? $url_logo["sizes"]["large"] : IMAGES_DIRECTORY . '/logo.png';

$url_favicon 		= get_field("theme_settings_general__favicon", "option");
$favicon 			= ($url_favicon) ? $url_favicon["sizes"]["large"] : IMAGES_DIRECTORY . '/favicon.ico';

$global_phone 		= get_field("theme_settings_general__phone", "option");
$global_email 		= get_field("theme_settings_general__mail", "option");

$toggle_icon_selected = get_field("theme_settings_header__header_button_toggle_animation_type", "option");

$theme_settings_header__animation_header = get_field("theme_settings_header__animation_header", "option");

$classesAnimationHeader 	= $theme_settings_header__animation_header . " animated vdr-animated--level-2";

$is_home = is_front_page() ? " is-front" : " not-front";

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns:og="http://ogp.me/ns" xmlns:fb="http://ogp.me/ns/fb">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="<?php echo $favicon; ?>">
    <link rel="apple-touch-icon" href="<?php echo $favicon; ?>"/>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600&display=swap" rel="stylesheet">
   
    <?php wp_head(); ?>
</head>

<body <?php body_class($is_home); ?>><?php do_action('after_body'); ?>

	<!-- PAGE -->
	
	<div id="vdr-page" class="c-site">

		<div class="c-page-wrapper">

			<div class="c-top-bar">
				<div class="o-container o-container--no-padding o-container--header o-flex o-flex--right">
					<div class="c-secondary-nav">
					  	<?php

					        wp_nav_menu( array(
					            'theme_location' 	=>  'header_menu_secundary',
					            'container'     	=>  '',
					            'items_wrap'    	=> '<ul class="%2$s">%3$s</ul>',
					            'menu_class'    	=> 'o-menu c-secondary-nav-menu o-flex menu-depth-0'
					        ));

					  	?>
					</div>
					<div class="c-top-bar__rrss">
					
						<a href="#" class="c-top-bar__rrss-icon"> <img src="/wp-content/uploads/2019/11/facebook.png"></a>
						<a href="#" class="c-top-bar__rrss-icon"> <img src="/wp-content/uploads/2019/11/twitter.png"></a>
						<a href="#" class="c-top-bar__rrss-icon"> <img src="/wp-content/uploads/2019/11/instagram.png"></a>
						<a href="#" class="c-top-bar__rrss-icon"> <img src="/wp-content/uploads/2019/11/linkedin.png"></a>
					</div>
				</div>
			</div>

			<!-- MOBILE MENU -->
			<div class="c-mobile-menu js-mobile-menu">
				<div class="o-container o-container--no-padding o-flex c-mobile-menu__container">
					<button class="hamburger hamburger--collapse o-hide-desktop js-hamburger c-mobile-menu__hamburger" type="button">
					  	<span class="hamburger-box">
					    	<span class="hamburger-inner"></span>
					  	</span>
					</button>
				  	<nav class="c-mobile-menu__nav" role="navigation">
				  		<?php

					        wp_nav_menu( array(
					            'theme_location' 	=>  'header_menu',
					            'container'     	=>  '',
					            'items_wrap'    	=> '<ul class="%2$s">%3$s</ul>',
					            'menu_class'    	=> 'o-menu c-mobile-menu__menu menu-depth-0'
					        ));

					  	?>
				  	</nav>

				  	<div class="o-container o-container--no-padding o-flex o-flex--between c-mobile-menu__rrss">
					
						<a href="#" class="c-mobile-menu__rrss-icon"> <img src="/wp-content/uploads/2019/11/facebook.png"></a>
						<a href="#" class="c-mobile-menu__rrss-icon"> <img src="/wp-content/uploads/2019/11/twitter.png"></a>
						<a href="#" class="c-mobile-menu__rrss-icon"> <img src="/wp-content/uploads/2019/11/instagram.png"></a>
						<a href="#" class="c-mobile-menu__rrss-icon"> <img src="/wp-content/uploads/2019/11/linkedin.png"></a>
					</div>
				  </div>
			</div>
			<header id="vdr_header" class="c-header c-site-header <?php echo $classesAnimationHeader; ?>" role="banner">

				<!-- CONTAINER -->
				<div class="o-container o-container--no-padding o-container--header">

					<!-- CONTENT HEADER -->
					<div class="c-header__wrapper o-flex o-flex--between">

						<div class="c-header__logo">
							<?php include(locate_template('template-parts/template-part-block-logo.php')); ?>
						</div>

						<!-- HEADER BLOCK -->
						<div class="c-header__block">

							<!-- SEARCH FORM -->
							<!--
							<div class="c-header__search">
								<?php include(locate_template('template-parts/template-part-search.php')); ?>
							</div>
							-->

							<!-- ICON TOGGLE MENU MOBILE -->
							<button class="hamburger hamburger--collapse o-hide-desktop js-hamburger" type="button">
							  	<span class="hamburger-box">
							    	<span class="hamburger-inner"></span>
							  	</span>
							</button>

							<!-- MENU DESKTOP -->
							<div class="c-desktop-menu">
								<nav class="c-desktop-menu__nav" role="navigation">
								  	<?php

								        wp_nav_menu( array(
								            'theme_location' 	=>  'header_menu',
								            'container'     	=>  '',
								            'items_wrap'    	=> '<ul class="%2$s">%3$s</ul>',
								            'menu_class'    	=> 'o-menu c-desktop-menu__menu o-flex o-flex--right menu-depth-0'
								        ));

								  	?>
								</nav>
							</div>
							
						</div>

					</div>
					
				</div>
				
			</header>
			<!-- END HEADER -->

			<!-- MAIN -->
			<main id="vdr-content" class="c-site-content">