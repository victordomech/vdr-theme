<div class="c-spinner-wrapper" style="position: fixed;top: 0;left: 0;right: 0;bottom: 0;background-color: #71a6cf;z-index: 999999;display: -webkit-box;display: -ms-flexbox;display: -webkit-flex;display: flex;-webkit-box-pack: center;-ms-flex-pack: center;-webkit-justify-content: center;justify-content: center;-webkit-box-align: center;-ms-flex-align: center;-webkit-align-items: center;align-items: center;width: 100%; height: 100%; ">
	<div class="c-spinner">
	  <div class="c-spinner__dot1"></div>
	  <div class="c-spinner__dot2"></div>
	</div>
</div>
