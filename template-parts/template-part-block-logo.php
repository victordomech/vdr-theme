<div class="o-logo">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="o-logo__link c-header__logo-link"> 
		<img class="logo" class="o-logo__img" src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
	</a>
</div>