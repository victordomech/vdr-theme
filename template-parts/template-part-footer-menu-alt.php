<div class="c-footer-menu-alt">
	<?php
		wp_nav_menu(array(
				'theme_location' => 'footer_menu_alt',
				'container'      => '',
				'items_wrap'    => '<ul class="%2$s">%3$s</ul>',
				'menu_class'     => 'c-footer-menu-alt__wrapper',
			)
		);
	?>
</div>