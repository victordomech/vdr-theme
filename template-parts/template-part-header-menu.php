<div class="c-header-menu">

	<div class="c-toggle-mobile-menu">
    	<i class="fa fa-bars"></i>
  	</div>

  	<div class="c-content-nav">
        <nav role="navigation">
          	<?php

	            wp_nav_menu( array(
	                'theme_location' 	=>  'header_menu',
	                'container'     	=>  '',
	                'items_wrap'    	=> '<ul class="%2$s">%3$s</ul>',
	                'menu_class'    	=> 'c-menu c-header-menu__nav menu-depth-0',
	                'walker'  			=> new WPDocs_Walker_Nav_Menu() // custom walker
	            ));

          	?>
        </nav>

  	</div>

</div>