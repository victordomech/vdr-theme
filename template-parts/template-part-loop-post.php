<?php 

$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'large');
$image = $image[0];

$the_permalink = get_permalink(get_the_ID());

$animation_for_post = $animation_for_post__odd;

if($m % 2 == 0){

$animation_for_post = $animation_for_post__even;

}

$the_content = get_the_content();

?>

<article class="c-grid__item">
	<a class="c-grid__item-link" href="<?php the_permalink(); ?>">
		<div class="c-grid__item-image" style="background-image: url(<?php echo $image; ?>);">
			<div class="c-grid__item-icon">
				<span class="fa fa-plus animated"></span>
			</div>
		</div>
		<div class="c-grid__item-title">
			<h2><?php echo get_the_title(); ?></h2>
		</div>
	</a>
</article>
