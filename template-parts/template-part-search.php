<div class="o-search js-search">
	<form class="o-search__form" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<?php
			printf( '<input class="o-search__form-input" type="text"  value="%1$s" name="s" title="%2$s" />',
				get_search_query(),
				esc_attr__( 'Search for:', THEME_NAME )
			);
		?>
		<div class="o-search__button">
			<span class="fa fa-search js-search__button"></span>
		</div>
	</form>
</div>