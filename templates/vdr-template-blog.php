<?php

/*
Template Name: VDR - Blog
*/

get_header();

the_post();

// FEATURED IMAGE
$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),'single-post-thumbnail');
$image = $image[0];


?>

	<section class="c-template-blog">
		<?php echo get_the_title(); ?>
	</section>

<?php

get_footer(); 

?>