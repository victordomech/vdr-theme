<?php

/*
Template Name: VDR - Home
*/

get_header();

the_post();

// FEATURED IMAGE
$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),'single-post-thumbnail');
$image = $image[0];


?>

	<div class="c-template-home">

	<?php if( have_rows('home-slider') ): ?>

	<section class="o-slider c-home-slider">
		<div class="c-home-slider__container js-home-slider">
			<div class="swiper-wrapper">

				<?php while( have_rows('home-slider') ): the_row(); 

					// vars
					$title = get_sub_field('home-slider_title');
					$subtitle = get_sub_field('home-slider_subtitle');
					$link = get_sub_field('home-slider_button');
					$link_url = $link['url'];
				    $link_title = $link['title'];
				    $link_target = $link['target'] ? $link['target'] : '_self';
					$background = get_sub_field('home-slider_desktop');
					?>

					<div class="swiper-slide o-slider__item c-home-slider__item o-flex o-flex--right" style="background-image: url(<?= $background ?>);">

						<div class="c-home-slider__content">
							
							<h2 class="c-home-slider__content-title"><?= $title ?></h2>
							<p class="c-home-slider__content-subtitle"><?= $subtitle ?></p>
							<?php if( $link ): ?>
								<a class="c-home-slider__content-button" href=" <?php echo esc_url( $link_url ); ?> " target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
							<?php endif; ?>
						</div>
					</div>

				<?php endwhile; ?>
			</div>
			<!-- Add Arrows -->
		    <div class="o-slider__arrow o-slider__arrow--next js-home-slider__arrow-next"><i class="fas fa-chevron-right"></i></div>
		    <div class="o-slider__arrow o-slider__arrow--prev js-home-slider__arrow-prev"><i class="fas fa-chevron-left"></i></div>
		</div>
	</section>
	<section class="o-slider c-home-into">
		<div class="o-container">

			<h2 class="c-home-into__title o-text__title o-text__center"><?php the_field('home-into_title'); ?></h2>
			<p class="c-home-into__subtitle o-text__center"><?php the_field('home-into_subtitle'); ?></p>

			<div class="c-home-into__advert-container o-flex o-flex--between ">
				<?php while( have_rows('home-into_advert') ): the_row(); 

					// vars
					$title = get_sub_field('home-into_advert-title');
					$subtitle = get_sub_field('home-into_advert-subtitle');
					$icon = get_sub_field('home-into_advert-icon');
				
					?>

					<div class="c-home-into__advert o-text__center">
						<img src="<?= $icon ?>" alt="icon" class="c-home-into__advert-img">
						<h3 class="c-home-into__advert-title"> <?= $title ?></h3>
						<p class="c-home-into__advert-subtitle"> <?= $subtitle ?></p>
					</div>

				<?php endwhile; ?>

			</div>
		</div>
	</section>
	<section class="c-home-parallax" style="background-image: url(<?php the_field('home-parallax_img'); ?>">

		<div class="o-container o-flex o-flex--right">
			<div class="c-home-parallax-wrapper">
			 	<h2 class="c-home-parallax-title"><?php the_field('home-parallax_title'); ?></h2>
			 	<?php while( have_rows('home-parallax_advert') ): the_row(); 

					// vars
					$title = get_sub_field('home-parallax_advert-Title');
					$subtitle = get_sub_field('home-parallax_advert-subtitle');
					$icon = get_sub_field('home-parallax_advert-icon');
				
					?>

					<div class="c-home-parallax__advert o-flex o-flex--top">

						<div class="c-home-parallax__icon">
							<?= $icon ?>
						</div>
						<div class="c-home-parallax__text">
							<h3 class="c-home-parallax__advert-title"> <?= $title ?></h3>
							<p class="c-home-parallax__advert-subtitle"> <?= $subtitle ?></p>
						</div>
					</div>

				<?php endwhile; ?>
			</div>
		</div>
	</section>

	<section class="c-home-team">
		<div class="o-container o-flex o-flex--between">
			<div class="c-home-team__item o-text__center">
				<a href="" class="c-home-team__item-link"><div class="c-home-team__item-img"><img src="<?php the_field('home-team_img1'); ?>"></div></a>
				<div class="c-home-team__item-name"><?php the_field('home-team_name1'); ?></div>
				<div class="c-home-team__item-text"><?php the_field('home-team_text1'); ?></div>
			</div>
			<div class="c-home-team__item c-home-team__item--big o-text__center">
				<h2 class="c-home-team__item-title o-text__title"><?php the_field('home-team_title'); ?></h2>
				<div class="c-home-team__item-content"><?php the_field('home-team_text'); ?></div>
				<?php
					$link = get_field('home-team_button');
					$link_url = $link['url'];
				    $link_title = $link['title'];
				    $link_target = $link['target'] ? $link['target'] : '_self';
			    ?>
			    <a class="c-home-team__item-button" href=" <?php echo esc_url( $link_url ); ?> " target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			</div>
			<div class="c-home-team__item o-text__center">
				<a href="" class="c-home-team__item-link"><div class="c-home-team__item-img"><img src="<?php the_field('home-team_img2'); ?>"></div></a>
				<div class="c-home-team__item-name"><?php the_field('home-team_name2'); ?></div>
				<div class="c-home-team__item-text"><?php the_field('home-team_text2'); ?></div>
			</div>
		</div>
	</section>

	<?php endif; ?>
		

	</div>

<?php

get_footer(); 

?>